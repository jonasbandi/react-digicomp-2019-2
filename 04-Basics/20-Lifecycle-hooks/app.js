function Counter() {
  console.log('Counter rendering ...');

  const [count, setCount] = React.useState(0);

  React.useEffect(() => {
    console.log('Running effect ...');
  });

  console.log('Counter returning.');
  return (
    <div>
      <h3>Lifecycle Example</h3>
      <p>
        Count:
        <b> {count} </b>
      </p>
    </div>
  );
}

function App() {
  const [showCounter, setShowCounter] = React.useState(true);

  function toggle(){
    setShowCounter(!showCounter);
  }

  return (
    <div>
      <button onClick={toggle}>Toggle!</button>
      {showCounter ? <Counter /> : null}
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));


// DEMO:
// - show useEffect
// - toggle the rendering of Counter
// - call setCount(count + 1) in effect => endless loop  - btw: no loop if just calling setCount(count) ...
// - add [] to use effect => effect only called once
// - add [count] to use effect => endless loop is back
// - call setInterval(() => setCount(count + 1), 1000) => count does not increase! Why? -> Closure! (log the count in the interval handler)
// - add [count] -> very bad!
// - return cleanup -> not elegant
// - better: setInterval(() => setCount(count => count + 1), 1000)
//
// - periodically update the App component (using also an effect) -> effect in Counter is run

// Class based lifecycles as effect:
//
//   React.useEffect(() => {
//     console.log('Counter did mount.');
//     return () => console.log('Counter did unmount');
//   }, []);
//
//   React.useEffect(() => {
//     console.log('Counter did update.');
//   });
