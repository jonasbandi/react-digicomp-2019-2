import {Container} from 'unstated';

export class ToDoStore extends Container {
  state = {
    todos: [
      {id: 1, title: 'Learn React'},
      {id: 2, title: 'Learn Redux'}
    ],
  };

  addToDo = (newToDo) => {
    const newToDos = [...this.state.todos, newToDo];
    this.setState({todos: newToDos });
  };

  removeToDo = (toDo) => {
    const newToDos = this.state.todos.filter(t => t !== toDo);
    this.setState({todos: newToDos});
  };
}
