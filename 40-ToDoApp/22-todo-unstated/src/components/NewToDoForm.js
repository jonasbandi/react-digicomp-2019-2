import React from 'react';
import {PropTypes} from 'prop-types'
import {connectToStore} from '../storeUtil';
import {ToDoStore} from '../ToDoStore';

class NewToDoForm extends React.Component {

  state = {
    toDoTitle: ''
  };

  addToDo = (e) => {
    e.preventDefault();
    const newToDo = {id: Math.random(), title: this.state.toDoTitle}
    this.props.store.addToDo(newToDo);
    this.setState({toDoTitle: ''});
  };

  render() {
    return (
      <form className="new-todo" onSubmit={this.addToDo}>
        <input id="todo-text" name="toDoTitle" type="text" placeholder="What needs to be done?"
               autoFocus
               autoComplete="off"
               value={this.state.toDoTitle}
               onChange={this.formChange}
        />
        <button id="add-button" className="add-button">+</button>
      </form>
    );
  }

  formChange = (e) => {
    this.setState({[e.currentTarget.name]: e.currentTarget.value});
  };

}

export default connectToStore([ToDoStore])(NewToDoForm);
