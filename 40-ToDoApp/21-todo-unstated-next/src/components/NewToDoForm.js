import React, {useState} from 'react';
import TodoStore from '../TodoStore';

export default function NewToDoForm() {

  const [toDoTitle, setToDoTitle] = useState('');
  const todoStore = TodoStore.useContainer();

  function formChange(e) {
    setToDoTitle(e.currentTarget.value);
  }

  function addToDo(e) {
    e.preventDefault();
    todoStore.addToDo(toDoTitle);
    setToDoTitle('');
  }

  return (
    <form className="new-todo" onSubmit={addToDo}>
      <input id="todo-text" name="toDoTitle" type="text" placeholder="What needs to be done?"
             autoFocus
             autoComplete="off"
             value={toDoTitle}
             onChange={formChange}
      />

      <button id="add-button" className="add-button">+</button>
    </form>
  );
}


