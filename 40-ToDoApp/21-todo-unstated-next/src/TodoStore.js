import {useState} from 'react';
import {loadToDos, storeToDos} from './persistence';
import {createContainer} from 'unstated-next';

function useTodos(initialState = []) {

  const [todos, setTodos] = useState(() => loadToDos());

  function addToDo(title) {
    const newToDos = [...todos, {id: Math.random(), title: title, completed: false}];
    setTodos(newToDos);
    storeToDos(newToDos);
  }

  function removeToDo(toDo) {
    const newToDos = todos.filter(t => t !== toDo);
    setTodos(newToDos);
    storeToDos(newToDos);
  }

  function initToDos() {
    let todos = loadToDos();
    setTodos(todos);
  }

  return {todos, addToDo, removeToDo, initToDos}
}

let TodoStore = createContainer(useTodos);

export default TodoStore;
