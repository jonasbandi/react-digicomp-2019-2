import axios from 'axios';

const API_URL = 'http://localhost:3456/todos';

export function loadToDos() {
    return axios.get(API_URL)
        .then(resp => resp.data.result);
}

export function saveToDo(toDo) {
    return axios.post(API_URL, toDo)
      .then(resp => resp.data.result);
}

export function deleteToDo(toDo) {
    return axios.delete(`${API_URL}/${toDo.id}`);
}


