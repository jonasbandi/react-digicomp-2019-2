import * as React from 'react';
import { IToDo } from '../model/ToDo';

type ToDoListItemProps = {
    todo: IToDo,
    onRemoveToDo(todo: IToDo): void
}

function ToDoListItem({todo, onRemoveToDo}: ToDoListItemProps) {

    let removeButton = null;
    if (todo.id) {
        removeButton = <button onClick={() => onRemoveToDo(todo)}>X</button>
    }

    return (
        <li key={todo.id}>
            {todo.title}
            {removeButton}
        </li>
    );
}

export default ToDoListItem;
