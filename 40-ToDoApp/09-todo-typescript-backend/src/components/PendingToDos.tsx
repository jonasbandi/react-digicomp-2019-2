import * as React from 'react';
import NewToDoForm from './NewToDoForm';
import ToDoList from './ToDoList';
import { loadToDos, saveToDo, updateToDo } from '../model/persistence';
import { IToDo } from '../model/ToDo';

type ToDoScreenState = {
    loadingMessage: string | undefined;
    todos: IToDo[];
}

class PendingToDos extends React.Component<{}, ToDoScreenState> {

    state: ToDoScreenState = {
        loadingMessage: 'Loading ...',
        todos: []
    };

    render() {

        const loadingIndicator = this.state.loadingMessage ? <div>{this.state.loadingMessage}</div> : null;

        return (
            <div>
                {loadingIndicator}
                <NewToDoForm onAddToDo={this.addToDo}/>

                <div className="main">
                    <ToDoList todos={this.state.todos} onRemoveToDo={this.completeToDo}/>
                </div>
            </div>
        );
    }

    async componentDidMount() {
        const todos = await loadToDos();
        this.setState({todos, loadingMessage: undefined});
    }

    addToDo = async (title: string) => {
        const newToDo = {id: undefined, title, completed: false};
        const newToDos = [...this.state.todos, newToDo];

        // "OPTIMISTIC UI"
        this.setState({todos: newToDos, loadingMessage: 'Saving ...'});
        // this.setState({ loadingMessage: 'Saving ...'});

        // saveToDo(newToDo)
        //   .then(todo => {
        //     const updatedToDos = this.state.todos.map(t => t !== newToDo ? t : todo);
        //     // const updatedToDos = [...this.state.todos, todo];
        //     this.setState({todos: updatedToDos, loadingMessage: null});
        //   });

        const persistedToDo = await saveToDo(newToDo);
        const updatedToDos = this.state.todos.map(t => t !== newToDo ? t : persistedToDo);
        this.setState({todos: updatedToDos, loadingMessage: undefined});

    };

    completeToDo = (toDo: IToDo) => {
        toDo.completed = true;

        // "OPTIMISTIC UI"
        const updatedToDos = this.state.todos.filter(t => t.id !== toDo.id);
        this.setState({todos: updatedToDos, loadingMessage: 'Saving ...'})

        updateToDo(toDo)
            .then(todo => {
                this.setState({loadingMessage: undefined});
            });

    };


}

export default PendingToDos;