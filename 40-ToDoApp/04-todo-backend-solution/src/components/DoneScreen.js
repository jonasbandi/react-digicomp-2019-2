import React, {useState, useEffect} from 'react';
import ToDoList from './ToDoList';
import {loadToDos, deleteToDo} from '../persistence';

function DoneScreen() {

  const [todos, setTodos] = useState([]);

  useEffect(() => {
    // using an IIFE since the effect function is not allowed to return a value (exept the cleanup function)
    // and async functions implicitly return a promise
    (async () => {
      const todos = await loadToDos(1);
      setTodos(todos);
    })();
  }, []);

  function removeToDo(toDo) {

    // "OPTIMISTIC UI"
    const newToDos = todos.filter(t => t.id !== toDo.id);
    updateToDos(newToDos);

    deleteToDo(toDo);
  }

  function updateToDos(updatedToDos) {
    const pendingToDos = updatedToDos.filter(t => t.completed)
    setTodos(pendingToDos);
  }

  return (
    <div>
      <div className="main">
        <ToDoList todos={todos} onRemoveToDo={removeToDo}/>
      </div>
    </div>
  );
}

export default DoneScreen;
