import React, {useState, useEffect} from 'react';
import NewToDoForm from './NewToDoForm';
import ToDoList from './ToDoList';
import {loadToDos, saveToDo, updateToDo} from '../persistence';

function ToDoScreen() {

  const [todos, setTodos] = useState([]);
  const [message, setMessage] = useState('');

  useEffect(() => {
    // using an IIFE since the effect function is not allowed to return a value (exept the cleanup function)
    // and async functions implicitly return a promise
    (async () => {
      setMessage('Loading ...');
      const todos = await loadToDos();
      setTodos(todos);
      setMessage('');
    })();
  }, []);

  async function addToDo(title) {

    const newToDo = {title: title, completed: false};

    // "OPTIMISTIC UI"
    const newToDos = [...todos, newToDo];
    setTodos(newToDos);
    setMessage('Saving ...');
    const persistedToDo = await saveToDo(newToDo);
    const updatedToDos = newToDos.map(t => t !== newToDo ? t : persistedToDo);
    setTodos(updatedToDos);
    setMessage('');

    // "PESSIMISTIC UI"
    // setMessage('Saving ...');
    // const persistedToDo = await saveToDo(newToDo);
    // const newToDos = [...todos, persistedToDo];
    // setTodos(newToDos);
    // setMessage('');
  }

  async function completeToDo(toDo) {
    toDo.completed = true;

    // "OPTIMISTIC UI"
    const updatedToDos = todos.filter(t => t.id !== toDo.id);
    setTodos(updatedToDos);
    setMessage('Saving ...');
    await updateToDo(toDo);
    setMessage('');

    // "PESSIMISTIC UI"
    // setMessage('Saving ...');
    // await updateToDo(toDo);
    // const updatedToDos = todos.filter(t => t.id !== toDo.id);
    // setTodos(updatedToDos);
    // setMessage('');
  }

  let loadingIndicator =
    message
      ? <div>{message}</div>
      : null;

  return (
    <div>
      {loadingIndicator}
      <NewToDoForm onAddToDo={addToDo}/>

      <div className="main">
        <ToDoList todos={todos} onRemoveToDo={completeToDo}/>
      </div>
    </div>
  );
}

export default ToDoScreen;
