import axios from 'axios';

// const API_URL = 'http://localhost:3456/todos';
const API_URL = 'https://jba-todo.now.sh/api/todos';

export async function loadToDos(completed = 0) {
  const serverResponse = await axios.get(API_URL, { params: { completed }});
  return serverResponse.data.data;
}

export async function saveToDo(toDo) {
  try {
    const serverResponse = await axios.post(API_URL, toDo);
    return serverResponse.data.data;
  } catch {
    alert('Something went terribly wrong!');
    window.location.reload();
  }
}

export async function updateToDo(toDo) {
  try {
    await axios.put(`${API_URL}/${toDo.id}`, toDo);
  } catch(error) {
    console.log(error)
  }
}

export async function deleteToDo(toDo) {
  try {
    await axios.delete(`${API_URL}/${toDo.id}`);
  } catch(error) {
    console.log(error)
  }
}
