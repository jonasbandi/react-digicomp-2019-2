import React, { lazy, Suspense } from 'react';
import './App.scss';
import { NavLink, Route, BrowserRouter as Router, Switch } from 'react-router-dom';

// Note: code-splitting with dynamic import() is optional. The componets could also be imported statically.
const About = lazy(() => import('./about/About'));
const Home = lazy(() => import('./home/Home'));

const App: React.FC = () => {
  return (
      <div className="App">
        <Router>
          <header className="App-header">
            <ul>
              <li>
                <NavLink to="/" exact>Home</NavLink>
              </li>
              <li>
                <NavLink to="/about" exact>About</NavLink>
              </li>
            </ul>
          </header>
          <div className="App-body">
            <Suspense fallback={<h3>Loading ...</h3>}>
              <Switch>
                <Route path="/about">
                  <About/>
                </Route>
                <Route path="/">
                  <Home/>
                </Route>
              </Switch>
            </Suspense>
          </div>
        </Router>
      </div>
  );
}

export default App;
