import React, { useState } from 'react';

export function Greeter({message}: { message: string }) {

    const [name, setName] = useState('');

    return (
        <div>
            <h3>{message} {name}</h3>
            <input value={name} onChange={(e) => setName(e.target.value)}/>
        </div>
    )
}
