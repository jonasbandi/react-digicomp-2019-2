import React from 'react';
import {useParams, useLocation, useRouteMatch, useHistory} from 'react-router-dom';

import styles from './About.module.scss';


function About() {

  const params = useParams();
  const location = useLocation();
  const match = useRouteMatch();
  const history = useHistory();

  return (
    <div className={styles.comic}>
      <h1>About</h1>
      <p>This is the About component.</p>
      <br/>
      <div>Router info:</div>
      <pre>{JSON.stringify(params)}</pre>
      <pre>{JSON.stringify(location)}</pre>
      <pre>{JSON.stringify(match)}</pre>
      <button onClick={() => history.push('/')}>Go Home</button>
    </div>
  );
}

export default About;
