import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { render, fireEvent, cleanup, waitForElement } from '@testing-library/react';
import { Greeter } from './Greeter';

describe('Greeter component', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Greeter />, div);
  });

  it('renders default message into the DOM', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Greeter />, div);
    // console.log(div.textContent);
    expect(div.innerHTML).toContain('Hello World');
  });

  it('renders default message into the virtual DOM', () => {
    const greeter = shallow(<Greeter />);
    const welcome = <div>Hello World!</div>;
    expect(greeter).toContainReact(welcome);
  });

  it('renders dynamic message (using react testing library)', async () => {
    const { getByText, getByTestId } = render(<Greeter />);

    fireEvent.change(getByTestId('name-input'), { target: { value: 'Test' } });

    // const greetingTextNode = await waitForElement(() => getByText(/Hello/i));

    expect(getByText(/Hello/i)).toHaveTextContent('Test');
  });

  it('renders dynamic message (using enzyme)', () => {
    const greeter = shallow(<Greeter />);

    greeter.find('input').simulate('change', { target: { value: 'Test' } });

    const welcome = <div>Hello Test!</div>;
    expect(greeter).toContainReact(welcome);
  });

  // Note: with class based components testing used to be easier
  // by calling to test instance methods using TestRenderer like this:
  // it('updates state', () => {
  //   const greeter = TestRenderer.create(<Greeter />).getInstance();
  //   greeter.handleChange({target: {value: 'Test'}});
  //   expect(greeter.state).toEqual({name: 'Test'});
  // });
});
