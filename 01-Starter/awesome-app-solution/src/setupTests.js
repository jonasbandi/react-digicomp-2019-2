import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';
import '@testing-library/jest-dom/extend-expect';

// this is a configuration file fro create-react-app
// it is used as a convention
// see https://create-react-app.dev/docs/running-tests

configure({ adapter: new Adapter() });
