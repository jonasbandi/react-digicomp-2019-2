function Topics() {
  const {url} = useRouteMatch();

  return (
    <div>
      <h2>Topics</h2>
      <ul>
        <li>
          <Link to={`${url}/1`}>
            Topic 1
          </Link>
        </li>
        <li>
          <Link to={`${url}/2`}>
            Topic 2
          </Link>
        </li>
        <li>
          <Link to={`${url}/3`}>
            Topic 3
          </Link>
        </li>
      </ul>

      <Switch>
        <Route path={`${url}/:topicId`}>
          <Topic/>
        </Route>
        <Route path={url}>
          <h3>Please select a topic.</h3>
        </Route>
      </Switch>
    </div>
  );
}
