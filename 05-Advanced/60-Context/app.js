const {useState, useContext} = React;

const TimeContext = React.createContext();

function TimeDisplay() {

  const date = useContext(TimeContext);

  return (
    <div>
      Time: {date.toLocaleTimeString()}
    </div>
  )
}

function TimeCard() {
  return (
    <div>
      <TimeDisplay/>
    </div>
  )
}

function TimeScreen() {
  return (
    <div>
      <TimeCard/>
    </div>
  )
}

function App() {

  const [date, setDate] = useState(new Date());

  function updateTime() {
    setDate(new Date())
  }

  return (

    <React.Fragment>
      <div>
        <button onClick={updateTime}>Update Time</button>
      </div>

      <TimeContext.Provider value={date}>
        <TimeScreen/>
      </TimeContext.Provider>

    </React.Fragment>
  )
}

ReactDOM.render(<App/>, document.getElementById('root'));


//  // Alternative before Hooks: Render Props
// function TimeDisplay() {
//   return (
//     <TimeContext.Consumer>
//       {(date) => (
//         <div>
//           Time: {date.toLocaleTimeString()}
//         </div>
//       )}
//     </TimeContext.Consumer>
//   )
// }
