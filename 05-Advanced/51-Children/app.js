function WrapperComponent(props) {
    return (
      <div className="container">
        <hr/>
        {props.children}
        <hr/>
      </div>
    )
}


function App() {
    return (
      <WrapperComponent>
        Test!
      </WrapperComponent>
    );
}

ReactDOM.render(<App/>, document.getElementById("root"));

